SUBDIRS =

bin_PROGRAMS = e16

e16_SOURCES = \
	E.h			\
	about.c			\
	aclass.c		aclass.h		\
	actions.c		\
	alert.c			alert.h			\
	animation.c		animation.h		\
	arrange.c		\
	backgrounds.c		backgrounds.h		\
	borders.c		borders.h		\
	buttons.c		buttons.h		\
	comms.c			comms.h \
	conf.h			\
	config.c		\
	container.c		container.h		\
	coords.c                \
	cursors.c		cursors.h		\
	desktops.c		desktops.h		\
	dialog.c	        dialog.h	        \
	dock.c			\
	draw.c			\
	econfig.c		econfig.h		\
	edebug.h		\
	edge.c			\
	eimage.c		eimage.h		\
	emodule.c		emodule.h		\
	eobj.c			eobj.h			\
	etypes.h		\
	events.c		events.h		\
	ewins.c			ewins.h			\
	ewin-ops.c		ewin-ops.h		\
	ewmh.c			\
	extinitwin.c		\
	file.c		        file.h		        \
	finders.c		\
	focus.c 		focus.h			\
	fonts.c 		\
	fx.c                    \
	grabs.c			grabs.h			\
	groups.c		groups.h		\
	handlers.c		\
	hints.c			hints.h			\
	hiwin.c			hiwin.h			\
	icccm.c			\
	iclass.c		iclass.h		\
	iconify.c		\
	icons.c			icons.h			\
	ipc.c			ipc.h \
	lang.c			lang.h			\
	list.c			list.h			\
	main.c			\
	memory.c		\
	menus.c			menus.h			\
	menus-misc.c		\
	misc.c			\
	mod-misc.c		\
	moveresize.c		\
	mwm.c			\
	pager.c                 \
	parse.c                 parse.h                 \
	progress.c		progress.h		\
	regex.c			\
	screen.c                screen.h                \
	session.c               session.h               \
	settings.c              settings.h              \
	setup.c			\
	shapewin.c		shapewin.h		\
	size.c			\
	slide.c			slide.h			\
	slideout.c		\
	snaps.c                 snaps.h			\
	sounds.h		\
	stacking.c		\
	startup.c		\
	string.c		\
	systray.c		\
	tclass.c		tclass.h		\
	text.c			\
	theme.c                 \
	time.c			\
	timers.c		timers.h		\
	tooltips.c		tooltips.h		\
	user.c			user.h			\
	util.h			\
	warp.c			\
	windowmatch.c   	windowmatch.h		\
	xprop.c			xprop.h	 xpropdefs.h	\
	x.c                     xwin.h			\
	xtypes.h

e16_SOURCES += text_xfs.c
if ENABLE_SOUND
e16_SOURCES += sound.c sound.h
endif
if ENABLE_ZOOM
e16_SOURCES += zoom.c
endif
if ENABLE_GLX
e16_SOURCES += eglx.c eglx.h glwin.c
endif
if ENABLE_COMPOSITE
e16_SOURCES += ecompmgr.c ecompmgr.h magwin.c
endif
if ENABLE_DBUS
e16_SOURCES += edbus.c edbus.h
endif

AM_CFLAGS = $(CFLAGS_WARNINGS) $(CFLAGS_VISIBILITY) $(CFLAGS_ASAN)

AM_CPPFLAGS = \
	-D LOCALEDIR=\"$(datadir)/locale\" \
	-D ENLIGHTENMENT_BIN=\"$(bindir)\" \
	-D ENLIGHTENMENT_LIB=\"$(pkglibdir)\" \
	-D ENLIGHTENMENT_ROOT=\"$(pkgdatadir)\"

e16_CPPFLAGS = \
	$(AM_CPPFLAGS) \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	$(SOUND_CFLAGS) \
	$(FONTS_CFLAGS) \
	$(IMLIB2_CFLAGS) \
	$(DBUS_CFLAGS) \
	$(XI_CFLAGS) \
	$(XRANDR_CFLAGS) \
	$(XPRESENT_CFLAGS) \
	$(X_CFLAGS)

e16_LDADD = \
	$(LTLIBINTL)		\
	$(LTLIBICONV)		\
	$(IMLIB2_LIBS)		\
	$(SOUND_LIBS)		\
	$(FONTS_LIBS)		\
	$(GLX_LIBS)		\
	$(XCOMPOSITE_LIBS)	\
	$(XDAMAGE_LIBS)		\
	$(XFIXES_LIBS)		\
	$(XRENDER_LIBS)		\
	$(XI_LIBS)		\
	$(XRANDR_LIBS)		\
	$(XPRESENT_LIBS)	\
	$(X_LIBS)		\
	$(SM_LIBS)		\
	$(XINERAMA_LIBS)	\
	$(XSCREENSAVER_LIBS)	\
	$(XXF86VM_LIBS)		\
	$(X_EXTRA_LIBS)		\
	$(E_X_LIBS)		\
	$(DBUS_LIBS)		\
	$(MODULE_LIBS)		\
	$(CLOCK_LIBS)		\
	-lX11 -lm

SNDLDR_CFLAGS = $(AUDIOFILE_CFLAGS) $(SNDFILE_CFLAGS)
SNDLDR_LIBS = $(AUDIOFILE_LIBS) $(SNDFILE_LIBS)

if BUILD_MODULES

e16_LDFLAGS = -export-dynamic
MODULE_LIBS = $(DLOPEN_LIBS)

libe16dir = $(pkglibdir)

libe16_LTLIBRARIES = $(LIBSND_ESOUND) $(LIBSND_PULSE) $(LIBSND_SNDIO) $(LIBSND_ALSA) $(LIBSND_PLAYER) $(LIBFNT_IFT) $(LIBFNT_XFT) $(LIBFNT_PANGO)

if USE_SOUND_ESOUND
LIBSND_ESOUND = libsound_esound.la
libsound_esound_la_SOURCES   = sound_esound.c sound_load.c
libsound_esound_la_CFLAGS    = $(ESD_CFLAGS) $(SNDLDR_CFLAGS)
libsound_esound_la_LIBADD    = $(ESD_LIBS) $(SNDLDR_LIBS)
libsound_esound_la_LDFLAGS   = -module -avoid-version
endif

if USE_SOUND_PULSE
LIBSND_PULSE = libsound_pulse.la
libsound_pulse_la_SOURCES = sound_pulse.c sound_load.c
libsound_pulse_la_CFLAGS  = $(PULSE_CFLAGS) $(SNDLDR_CFLAGS)
libsound_pulse_la_LIBADD  = $(PULSE_LIBS) $(SNDLDR_LIBS)
libsound_pulse_la_LDFLAGS = -module -avoid-version
endif

if USE_SOUND_SNDIO
LIBSND_SNDIO = libsound_sndio.la
libsound_sndio_la_SOURCES = sound_sndio.c sound_load.c
libsound_sndio_la_CFLAGS  = $(SNDIO_CFLAGS) $(SNDLDR_CFLAGS)
libsound_sndio_la_LIBADD  = $(SNDIO_LIBS) $(SNDLDR_LIBS)
libsound_sndio_la_LDFLAGS = -module -avoid-version
endif

if USE_SOUND_ALSA
LIBSND_ALSA = libsound_alsa.la
libsound_alsa_la_SOURCES = sound_alsa.c sound_load.c
libsound_alsa_la_CFLAGS  = $(ALSA_CFLAGS) $(SNDLDR_CFLAGS)
libsound_alsa_la_LIBADD  = $(ALSA_LIBS) $(SNDLDR_LIBS)
libsound_alsa_la_LDFLAGS = -module -avoid-version
endif

if USE_SOUND_PLAYER
LIBSND_PLAYER = libsound_player.la
libsound_player_la_SOURCES = sound_player.c
libsound_player_la_LDFLAGS = -module -avoid-version
endif

LIBFNT_IFT = libfont_ift.la
libfont_ift_la_SOURCES    = text_ift.c
libfont_ift_la_CFLAGS     = $(IMLIB2_CFLAGS)
libfont_ift_la_LIBADD     = $(IMLIB2_LIBS)
libfont_ift_la_LDFLAGS    = -module -avoid-version

if USE_LIBXFT
LIBFNT_XFT = libfont_xft.la
libfont_xft_la_SOURCES    = text_xft.c
libfont_xft_la_CFLAGS     = $(XFT_CFLAGS)
libfont_xft_la_LIBADD     = $(XFT_LIBS)
libfont_xft_la_LDFLAGS    = -module -avoid-version
endif

if USE_LIBPANGO
LIBFNT_PANGO = libfont_pango.la
libfont_pango_la_SOURCES  = text_pango.c
libfont_pango_la_CFLAGS   = $(PANGO_CFLAGS)
libfont_pango_la_LIBADD   = $(PANGO_LIBS) libfont_xft.la
libfont_pango_la_LDFLAGS  = -module -avoid-version
endif

else # BUILD_MODULES

if ENABLE_SOUND
if USE_SOUND_LOADER
e16_SOURCES += sound_load.c
endif
if USE_SOUND_ESOUND
e16_SOURCES += sound_esound.c
endif
if USE_SOUND_PULSE
e16_SOURCES += sound_pulse.c
endif
if USE_SOUND_SNDIO
e16_SOURCES += sound_sndio.c
endif
if USE_SOUND_ALSA
e16_SOURCES += sound_alsa.c
endif
if USE_SOUND_PLAYER
e16_SOURCES += sound_player.c
endif
endif # ENABLE_SOUND

e16_SOURCES += text_ift.c
if USE_LIBXFT
e16_SOURCES += text_xft.c
endif
if USE_LIBPANGO
e16_SOURCES += text_pango.c
endif

SOUND_LIBS = $(ESD_LIBS) $(PULSE_LIBS) $(SNDIO_LIBS) $(ALSA_LIBS) $(SNDLDR_LIBS)
FONTS_LIBS = $(PANGO_LIBS) $(XFT_LIBS)

SOUND_CFLAGS = $(ESD_CFLAGS) $(PULSE_CFLAGS) $(SNDIO_CFLAGS) $(SNDLDR_CFLAGS)
FONTS_CFLAGS = $(PANGO_CFLAGS) $(XFT_CFLAGS)

endif # BUILD_MODULES

install-data-hook:
	rm -f $(DESTDIR)$(libe16dir)/*.la

uninstall-local:
	rm -f $(DESTDIR)$(libe16dir)/*.so
